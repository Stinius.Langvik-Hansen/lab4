package datastructure;

import cellular.CellState;

public class CellGrid implements IGrid {

    private int cols;
	private int rows;
	private CellState[][] agrid;

	public CellGrid(int rows, int columns, CellState initialState) {
		this.cols = columns;
		this.rows = rows;
		this.agrid = new CellState[rows][columns];
		for (int row = 0; row < rows; row++) {
			for (int col = 0; col < cols; col++) {
				this.agrid[row][col] = initialState;
			}
		}
		
	}

    @Override
    public int numRows() {
        return this.rows;
    }

    @Override
    public int numColumns() {
        return this.cols;
    }

    @Override
    public void set(int row, int column, CellState element) {
        this.agrid[row][column] = element;
        
    }

    @Override
    public CellState get(int row, int column) {
        return this.agrid[row][column];
    }

    @Override
    public IGrid copy() {
        CellGrid acopy = new CellGrid(this.rows, this.cols, null);
        for (int row = 0; row < this.rows; row++) {
        	for (int col = 0; col < this.cols; col++) {
        		acopy.agrid[row][col] = this.agrid[row][col];
				
			}
			
		}
        return acopy;
    }
    
}
